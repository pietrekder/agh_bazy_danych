#pragma once

#include <QWidget>
#include "ui_ProductsWidgets.h"
#include "DBhelper.h"
#include "DBDefines.h"
#include "ViewDataModels.h"

class ProductDataModel : public QAbstractTableModel
{
public:
};

class ProductsWidgets : public QWidget
{
    Q_OBJECT

public:
    ProductsWidgets(QWidget *parent = Q_NULLPTR);
    ~ProductsWidgets();

    void setDbHelper(DBD::DBhelper* helper_)
    {
        helper = helper_;
    }

    void init();

private:
    void selectAllProducts();
    void selectProductsById();
    void selectProductsFromCountry();

private:
    void initCountries();

private:
    Ui::ProductsWidgets ui;
    DBD::DBhelper* helper;

    ProductModel* model;
    std::vector<std::string> countries;
};

