#include "ViewDataModels.h"

ProductModel::ProductModel(QObject* parent)
    : QAbstractTableModel(parent)
{
}

int ProductModel::rowCount(const QModelIndex& parent) const
{
    return products.size();
}

int ProductModel::columnCount(const QModelIndex& parent) const
{
    return 10;
}

QVariant ProductModel::data(const QModelIndex& index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    auto r = index.row();
    auto c = index.column();

    if (r >= rowCount())
        return QVariant();
    if (c >= columnCount())
        return QVariant();

    auto p = products.at(r);

    switch (c)
    {
    case 0: return p.id;
    case 1: return QString(p.name.c_str());
    case 2: return p.supplier_id;
    case 3: return p.category_id;
    case 4: return QString(p.quantity_per_unit.c_str());
    case 5: return p.unit_price;
    case 6: return p.units_in_stock;
    case 7: return p.units_on_order;
    case 8: return p.reorder_level;
    case 9: return p.discontinued;
    default:
        return QVariant();
    }

    return QVariant();
}

QModelIndex ProductModel::index(int row, int column, const QModelIndex& parent) const
{
    return createIndex(row, column);
}

void ProductModel::insertProduct(DBDefines::Product& p)
{
    products.push_back(p);
    auto row = products.size();

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
    emit layoutChanged();
}

void ProductModel::clear()
{
    products.clear();

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
    emit layoutChanged();
}

QVariant ProductModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString("id");
        case 1:
            return QString("Name");
        case 2:
            return QString("SupplierId");
        case 3:
            return QString("CategoryId");
        case 4:
            return QString("Quantity\nper unit");
        case 5:
            return QString("unit price");
        case 6:
            return QString("unit stock");
        case 7:
            return QString("units order");
        case 8:
            return QString("reorder level");
        case 9:
            return QString("discontinued");
        }
    }
    return QVariant();
}





















OrderModel::OrderModel(QObject* parent)
    : QAbstractTableModel(parent)
{
}

int OrderModel::rowCount(const QModelIndex& parent) const
{
    return orders.size();
}

int OrderModel::columnCount(const QModelIndex& parent) const
{
    return 14;
}

QVariant OrderModel::data(const QModelIndex& index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    auto r = index.row();
    auto c = index.column();

    if (r >= rowCount())
        return QVariant();
    if (c >= columnCount())
        return QVariant();

    auto o = orders.at(r);

    switch (c)
    {
    case 0: return o.id;
    case 1: return QString(o.customer_id.c_str());
    case 2: return o.employee_id;
    case 3: return o.order_date.toString("dd-MM-yyyy hh:mm:ss");
    case 4: return o.required_date.toString("dd-MM-yyyy hh:mm:ss");
    case 5: return o.shipped_date.toString("dd-MM-yyyy hh:mm:ss");
    case 6: return o.ship_via;
    case 7: return QString(o.freight.c_str());
    case 8: return QString(o.ship_name.c_str());
    case 9: return QString(o.ship_address.c_str());
    case 10: return QString(o.ship_city.c_str());
    case 11: return QString(o.ship_region.c_str());
    case 12: return QString(o.ship_postcode.c_str());
    case 13: return QString(o.ship_country.c_str());
    default:
        return QVariant();
    }

    return QVariant();
}

QModelIndex OrderModel::index(int row, int column, const QModelIndex& parent) const
{
    return createIndex(row, column);
}

void OrderModel::insertOrder(DBDefines::Order& o)
{
    orders.push_back(o);
    auto row = orders.size();

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
    emit layoutChanged();
}

void OrderModel::clear()
{
    orders.clear();

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
    emit layoutChanged();
}

QVariant OrderModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString("id");
        case 1:
            return QString("Customer Id");
        case 2:
            return QString("Employee Id");
        case 3:
            return QString("Order date");
        case 4:
            return QString("Required date");
        case 5:
            return QString("Shipped date");
        case 6:
            return QString("Ship via");
        case 7:
            return QString("Freight");
        case 8:
            return QString("Ship name");
        case 9:
            return QString("Ship address");
        case 10:
            return QString("Ship city");
        case 11:
            return QString("Ship region");
        case 12:
            return QString("Ship postcode");
        case 13:
            return QString("Ship country");
        }
    }
    return QVariant();
}






















CustomerModel::CustomerModel(QObject* parent)
    : QAbstractTableModel(parent)
{
}

int CustomerModel::rowCount(const QModelIndex& parent) const
{
    return customers.size();
}

int CustomerModel::columnCount(const QModelIndex& parent) const
{
    return 11;
}

QVariant CustomerModel::data(const QModelIndex& index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    auto r = index.row();
    auto c = index.column();

    if (r >= rowCount())
        return QVariant();
    if (c >= columnCount())
        return QVariant();

    auto o = customers.at(r);

    switch (c)
    {
    case 0: return o.id;
    case 1: return o.companyname;
    case 2: return o.contactname;
    case 3: return o.contanttitle;
    case 4: return o.address;
    case 5: return o.city;
    case 6: return o.region;
    case 7: return o.postcode;
    case 8: return o.country;
    case 9: return o.phone;
    case 10: return o.fax;
    default:
        return QVariant();
    }

    return QVariant();
}

QModelIndex CustomerModel::index(int row, int column, const QModelIndex& parent) const
{
    return createIndex(row, column);
}

void CustomerModel::insertCustomer(DBDefines::Customer& c)
{
    customers.push_back(c);
    auto row = customers.size();

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
    emit layoutChanged();
}

void CustomerModel::clear()
{
    customers.clear();

    QModelIndex topLeft = index(0, 0);
    QModelIndex bottomRight = index(rowCount() - 1, columnCount() - 1);

    emit dataChanged(topLeft, bottomRight);
    emit layoutChanged();
}

QVariant CustomerModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role == Qt::DisplayRole && orientation == Qt::Horizontal) {
        switch (section) {
        case 0:
            return QString("id");
        case 1:
            return QString("Company name");
        case 2:
            return QString("Contact name");
        case 3:
            return QString("Contant title");
        case 4:
            return QString("Address");
        case 5:
            return QString("City");
        case 6:
            return QString("Region");
        case 7:
            return QString("Postcode");
        case 8:
            return QString("Country");
        case 9:
            return QString("Phone");
        case 10:
            return QString("Fax");
        }
    }
    return QVariant();
}