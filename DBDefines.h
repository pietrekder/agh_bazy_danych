#pragma once

#include <string>
#include <qdatetime.h>
#include <variant>
#include <optional>

namespace DBDefines
{
    namespace Fields
    {
        enum SupplierFields
        {
            SF_ID = 0,
            SF_COMPANY_NAME,
            SF_CONTACT_NAME,
            SF_CONTACT_TITLE,
            SF_ADDRESS,
            SF_CITY,
            SF_REGION,
            SF_POSTCODE,
            SF_COUNTRY,
            SF_PHONE,
            SF_FAX,
            SF_HOMEPAGE,
        };

        enum ProductFields
        {
            PF_ID = 0,
            PF_PRODUCTNAME,
            PF_SUPPLIER_ID,
            PF_CATEGORY_ID,
            PF_QUANTITY_PER_UNIT,
            PF_UNIT_PRICE,
            PF_UNITS_IN_STOCK,
            PF_UNITS_ON_ORDER,
            PF_REORDER_LEVEL,
            PF_DISCONTINUED,
        };
    }

    struct Supplier
    {
        int id;
        std::string company_name;
        std::string contact_name;
        std::string contact_title;
        std::string address;
        std::string city;
        std::string region;
        std::string postcode;
        std::string country;
        std::string phone;
        std::string fax;
        std::string homepage;
    };

    struct Category
    {
        int id;
        int supplier_id;
        int category_id;
        int quantity_per_unit;
        int unit_price;
        int units_in_stock;
        int units_on_order;
        int reorder_level;
        bool discontinued;
    };

    struct Product
    {
        int id;
        std::string name;
        int supplier_id;
        int category_id;
        std::string quantity_per_unit;
        float unit_price;
        int units_in_stock;
        int units_on_order;
        int reorder_level;
        bool discontinued;
    };

    struct Customer
    {
        QString id;
        QString companyname;
        QString contactname;
        QString contanttitle;
        QString address;
        QString city;
        QString region;
        QString postcode;
        QString country;
        QString phone;
        QString fax;
    };

    struct Order
    {
        int id;
        std::string customer_id;
        int employee_id;
        QDateTime order_date;
        QDateTime required_date;
        QDateTime shipped_date;
        int ship_via;
        std::string freight;
        std::string ship_name;
        std::string ship_address;
        std::string ship_city;
        std::string ship_region;
        std::string ship_postcode;
        std::string ship_country;
    };

    struct OrderDetails
    {
        int odid;
        int orderid;
        int productid;
        float unit_price;
        int quantity;
        float discount;
    };

    //DB access interface
    class IDBAccess
    {
    public:
        virtual bool Connect(const char* server,
                             const char* dbName,
                             const char* user,
                             const char* pwd) = 0;
        virtual void Disconnect() = 0;
        virtual bool IsOpen() = 0;

        //CRUD methods
        /* SUPPLIER */
        virtual bool InsertSupplier(Supplier& s) = 0;
        virtual std::optional<Supplier> GetSupplier(int sup_id) = 0;

        /* PRODUCT */
        virtual bool InsertProduct(Product& p) = 0;
        virtual std::optional<Product> GetProduct(int prod_id) = 0;

        /* ORDER */
        virtual int UpdateOrder(DBDefines::Order& order) = 0;

        /* SELECT PRODUCTS FROM COUNTRY */
        virtual int GetProductsFromCountry(std::vector<DBDefines::Product>& products, std::string country) = 0;

        /* DELETE (FOR NOW, FOR SIMPLICITY BY ID)*/
        virtual int DeleteEntry(std::string table_name, std::string id_name, int id) = 0;
    };
}
