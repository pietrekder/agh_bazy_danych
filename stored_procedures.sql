/* #01 */
/* INSERT SUPPLIER */
CREATE OR REPLACE FUNCTION InsertSupplier(_id int,
										  _company_name varchar(40),
										  _contact_name varchar(30),
										  _contact_title varchar(30),
										  _address varchar(60),
										  _city varchar(15),
										  _region varchar(15),
										  _postcode varchar(10),
										  _country varchar(15),
										  _phone char(24),
										  _fax char(24),
										  _homepage varchar)
	RETURNS int AS
	$BODY$
		BEGIN
			INSERT INTO suppliers(SupplierId, CompanyName, ContactName, ContactTitle, Address, City, Region, PostalCode, Country, Phone, Fax, Homepage)
			VALUES(_id, _company_name, _contact_name, _contact_title, _address, _city, _region, _postcode, _country, _phone, _fax, _homepage);
			RETURN (select MAX(suppliers.supplierid) from suppliers);
		END;
	$BODY$
	LANGUAGE 'plpgsql' VOLATILE
	COST 100;

	
	
/* #02 */
/* INSERT PRODUCT */
CREATE OR REPLACE FUNCTION 	InsertProduct(_id int,
										  _product_name varchar(40),
										  _supplier_id int,
										  _category_id int,
										  _quantity_per_unit int,
										  _unit_price float,
										  _units_in_stock int,
										  _units_on_order int,
										  _reorder_level int,
										  _discontinued smallint)
	RETURNS int AS
	$BODY$
		BEGIN
			INSERT INTO products(ProductId, ProductName, SupplierId, CategoryId, QuantityPerUnit, UnitPrice, UnitsInStock, UnitsOnOrder, ReorderLevel, Discontinued)
			VALUES(_id, _product_name, _supplier_id, _category_id, _quantity_per_unit, _unit_price, _units_in_stock, _units_on_order, _reorder_level, _discontinued);
			RETURN (select MAX(products.productid) from products);
		END;
	$BODY$
	LANGUAGE 'plpgsql' VOLATILE
	COST 100;

/* #03 */
/* INSERT ORDER */
CREATE OR REPLACE FUNCTION InsertOrder(_customer_id int,
									   _employee_id int,
									   _order_date timestamp,
									   _required_date timestamp,
									   _shipped_date timestamp,
									   _ship_via int,
									   _freight varchar,
									   _ship_address varchar,
									   _ship_city varchar,
									   _ship_region varchar,
									   _ship_postcode char(16),
									   _ship_country varchar)
	RETURNS int AS
	$BODY$
		BEGIN
			INSERT INTO orders(customer_id, employee_id, order_date, required_date, shipped_date, ship_via, freight, ship_address, ship_city, ship_region, ship_postcode, ship_country)
			VALUES(_customer_id, _employee_id, _order_date, _required_date, _shipped_date, _ship_via, _freight, _ship_address, _ship_city, _ship_region, _ship_postcode, _ship_country);
			RETURN (select MAX(orders.id) from orders);
		END;
	$BODY$
	LANGUAGE 'plpgsql' VOLATILE
	COST 100;


/* #04 */
/* UPDATE ORDER */
CREATE OR REPLACE FUNCTION UpdateOrder(_id int,
									   _customer_id int,
									   _employee_id int,
									   _order_date timestamp,
									   _required_date timestamp,
									   _shipped_date timestamp,
									   _ship_via int,
									   _freight varchar,
									   _ship_address varchar,
									   _ship_city varchar,
									   _ship_region varchar,
									   _ship_postcode char(16),
									   _ship_country varchar)
	RETURNS int AS
	$BODY$
	DECLARE update_count int := 0;
		BEGIN
			UPDATE orders
			SET customer_id=_customer_id, employee_id=_employee_id, order_date=_order_date, required_date=_required_date, shipped_date=_shipped_date, ship_via=_ship_via, freight=_freight, ship_address=_ship_address, ship_city=_ship_city, ship_region=_ship_region, ship_postcode=_ship_postcode, ship_country=_ship_country
			WHERE orders.id=_id;
			GET DIAGNOSTICS update_count = ROW_COUNT;
			RETURN update_count;
		END;
	$BODY$
	LANGUAGE 'plpgsql' VOLATILE
	COST 100;

















