#include "CustomersWidget.h"

CustomersWidget::CustomersWidget(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);

    model = new CustomerModel();
    ui.tableView->setModel(model);

    connect(ui.btn_all_cust, &QPushButton::pressed, this, [&]() { listCustomers(); });
}

CustomersWidget::~CustomersWidget()
{
}

void CustomersWidget::listCustomers()
{
    std::vector<DBDefines::Customer> customers;
    helper->GetAllCustomers(customers);

    for (auto& c : customers)
        model->insertCustomer(c);
}
