#pragma once

#include <qabstractitemmodel.h>
#include "DBDefines.h"

class ProductModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    ProductModel(QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    void insertProduct(DBDefines::Product& p);
    void clear();

private:
    std::vector<DBDefines::Product> products;
};



class OrderModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    OrderModel(QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    void insertOrder(DBDefines::Order& p);
    void clear();

private:
    std::vector<DBDefines::Order> orders;
};




class CustomerModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    CustomerModel(QObject* parent = nullptr);

    int rowCount(const QModelIndex& parent = QModelIndex()) const override;
    int columnCount(const QModelIndex& parent = QModelIndex()) const override;
    QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
    QModelIndex index(int row, int column, const QModelIndex& parent = QModelIndex()) const override;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

    void insertCustomer(DBDefines::Customer& c);
    void clear();

private:
    std::vector<DBDefines::Customer> customers;
};