#pragma once

#include <QWidget>
#include "ui_CustomersWidget.h"
#include "DBhelper.h"
#include "ViewDataModels.h"

class CustomersWidget : public QWidget
{
    Q_OBJECT

public:
    CustomersWidget(QWidget *parent = Q_NULLPTR);
    ~CustomersWidget();

    void setDbHelper(DBD::DBhelper* helper_)
    {
        helper = helper_;
    }

    void listCustomers();

private:
    Ui::CustomersWidget ui;
    DBD::DBhelper* helper;

    CustomerModel* model;
};
