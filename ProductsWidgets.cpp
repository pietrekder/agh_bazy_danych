#include "ProductsWidgets.h"
#include <qmessagebox.h>



ProductsWidgets::ProductsWidgets(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);
    model = new ProductModel();
    ui.tableView->setModel(model);

    connect(ui.btn_sel_by_id, &QPushButton::pressed, this, [&]() {selectProductsById(); });
    connect(ui.btn_country, &QPushButton::pressed, this, [&]() {selectProductsFromCountry(); });
    connect(ui.btn_allprod, &QPushButton::pressed, this, [&]() {selectAllProducts(); });
}

ProductsWidgets::~ProductsWidgets()
{
}

void ProductsWidgets::init()
{
    initCountries();
}

void ProductsWidgets::selectAllProducts()
{
    std::vector<DBDefines::Product> prods;
    QString country;
    if (ui.le_country->text().isEmpty())
        country = ui.comboBox->currentText();
    else
        country = ui.le_country->text();
    helper->GetAllProducts(prods);

    model->clear();
    for (auto& p : prods)
        model->insertProduct(p);
}

void ProductsWidgets::selectProductsById()
{
    bool ok;
    int id = ui.lineEdit->text().toInt(&ok);

    if (ok == false)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Id error");
        msgBox.setText("pole ID nie jest liczba");
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
        return;
    }

    auto r = helper->GetProduct(id);

    if (r.has_value() == false)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Product error");
        msgBox.setText("Produkt o podanym id nie istnieje!");
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
        return;
    }
    model->clear();
    auto p = r.value();
    model->insertProduct(p);
}

void ProductsWidgets::selectProductsFromCountry()
{
    std::vector<DBDefines::Product> prods;
    QString country;
    if (ui.le_country->text().isEmpty())
        country = ui.comboBox->currentText();
    else
        country = ui.le_country->text();
    helper->GetProductsFromCountry(prods, country.toStdString());

    model->clear();
    for (auto& p : prods)
        model->insertProduct(p);
}

void ProductsWidgets::initCountries()
{
    int s = helper->getSuppliersCountries(countries);

    for (auto& s : countries)
        ui.comboBox->addItem(QString(s.c_str()));
}
