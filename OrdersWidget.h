#pragma once

#include <QWidget>
#include "ui_OrdersWidget.h"
#include "DBhelper.h"

#include "ViewDataModels.h"

class OrdersWidget : public QWidget
{
    Q_OBJECT

public:
    OrdersWidget(QWidget *parent = Q_NULLPTR);
    ~OrdersWidget();

    void setDbHelper(DBD::DBhelper* helper_)
    {
        helper = helper_;
    }

    void listOrders();
    void makeOrder();

private:
    Ui::OrdersWidget ui;
    DBD::DBhelper* helper;

    OrderModel* model;
};
