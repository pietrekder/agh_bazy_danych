/* DELETING TABLES */
drop table if exists suppliers cascade;
drop table if exists products cascade;
drop table if exists categories cascade;
drop table if exists region cascade;
drop table if exists territories cascade;
drop table if exists employees cascade;
drop table if exists employee_territories cascade;
drop table if exists customer_demographics cascade;
drop table if exists customers cascade;
drop table if exists customercustomer_demo cascade;
drop table if exists shippers cascade;
drop table if exists orders cascade;
drop table if exists order_detail cascade;