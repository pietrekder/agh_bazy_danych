#pragma once

#include <QtWidgets/QMainWindow>
#include "ProductsWidgets.h"
#include "ui_PostgreSQL_DB.h"

class PostgreSQL_DB : public QMainWindow
{
    Q_OBJECT

public:
    PostgreSQL_DB(QWidget *parent = Q_NULLPTR);

private:
    Ui::PostgreSQL_DBClass ui;
    DBD::DBhelper* helper;
};
