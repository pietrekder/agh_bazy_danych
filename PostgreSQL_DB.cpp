#include "PostgreSQL_DB.h"

#include <qmessagebox.h>

PostgreSQL_DB::PostgreSQL_DB(QWidget *parent)
    : QMainWindow(parent)
{
    ui.setupUi(this);

    helper = new DBD::DBhelper("QPSQL");
    ui.tab_ProdWidg->setDbHelper(helper);
    ui.tab_Orders->setDbHelper(helper);
    ui.tab_Customers->setDbHelper(helper);

    connect(ui.pushButton, &QPushButton::pressed, this, [&]() {
        auto result = helper->Connect("localhost", "agh_northwnd", "postgres", "matrix777");

        if (result == false)
        {
            ui.label->setText("Error connecting to database");
            return;
        }
        ui.label->setText("Connection to database OK");
        ui.tab_ProdWidg->init();
    });
}
