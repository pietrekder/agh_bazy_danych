#include "DBhelper.h"


DBD::DBhelper::DBhelper(const char* driver)
{
    db = std::make_unique<QSqlDatabase>(QSqlDatabase::addDatabase(driver));
}

bool DBD::DBhelper::Connect(const char* server, const char* dbName, const char* user, const char* pwd)
{
	db->setConnectOptions();
	db->setHostName(server);
	db->setDatabaseName(dbName);
	db->setUserName(user);
	db->setPassword(pwd);

	if (db->open("postgres", "matrix777"))
		return true;
	return false;
}

void DBD::DBhelper::Disconnect()
{
	if (db->isOpen())
	{
		qDebug() << "closing DB";
		db->close();
	}
}

bool DBD::DBhelper::IsOpen()
{
	return (db != nullptr) ? db->open() : false;
}


/* DB Operations */

/*
*				InsertSupplier
*/
bool DBD::DBhelper::InsertSupplier(DBDefines::Supplier& s)
{
	if(db == nullptr)
		return false;

	auto id_opt = getMaxId("supplierid", "suppliers");
	int id;
	if (id_opt.has_value())
		id = id_opt.value() + 1;
	else
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("SELECT * from InsertSupplier(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}

	//query succeed
	query->addBindValue(id);
	query->addBindValue(s.company_name.c_str());
	query->addBindValue(s.contact_name.c_str());
	query->addBindValue(s.contact_title.c_str());
	query->addBindValue(s.address.c_str());
	query->addBindValue(s.city.c_str());
	query->addBindValue(s.region.c_str());
	query->addBindValue(s.postcode.c_str());
	query->addBindValue(s.country.c_str());
	query->addBindValue(s.phone.c_str());
	query->addBindValue(s.fax.c_str());
	query->addBindValue(s.homepage.c_str());

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return false;
	}

	if (query->next() == false)
		return false;

	s.id = query->value(0).toInt();

	return result;
}

/*
*				GetSupplier
*/
std::optional<DBDefines::Supplier> DBD::DBhelper::GetSupplier(int sup_id)
{
	if (db == nullptr)
		return std::nullopt;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("SELECT * from suppliers s where s.supplierid = ?")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return std::nullopt;
	}

	query->addBindValue(sup_id);

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return std::nullopt;
	}

	DBDefines::Supplier s;
	while (query->next())
	{
		using namespace DBDefines::Fields;
		s.id = query->value(SF_ID).toInt();
		s.company_name = query->value(SF_COMPANY_NAME).toString().toStdString();
		s.contact_name = query->value(SF_CONTACT_NAME).toString().toStdString();
		s.contact_title = query->value(SF_CONTACT_TITLE).toString().toStdString();
		s.address = query->value(SF_ADDRESS).toString().toStdString();
		s.city = query->value(SF_CITY).toString().toStdString();
		s.region = query->value(SF_REGION).toString().toStdString();
		s.postcode = query->value(SF_POSTCODE).toString().toStdString();
		s.country = query->value(SF_COUNTRY).toString().toStdString();
		s.phone = query->value(SF_PHONE).toString().toStdString();
		s.fax = query->value(SF_FAX).toString().toStdString();
		s.homepage = query->value(SF_HOMEPAGE).toString().toStdString();
	}

	return s;
}

bool DBD::DBhelper::InsertOrder(DBDefines::Order& o, std::vector<DBDefines::OrderDetails>& order_details)
{
	auto newodid = getMaxId("odid", "order_details");
	if (newodid.has_value() == false)
		return false;
	auto new_odid = newodid.value();

	auto newoid = getMaxId("orderid", "orders");
	if (newoid.has_value() == false)
		return false;
	o.id = newoid.value() + 1;


	for (auto& od : order_details)
	{
		od.orderid = o.id;
		new_odid++;
		od.odid = new_odid;
	}

	//insert order
	InsertSingleOrder(o);

	//insert order details
	for (auto& od : order_details)
		InsertOrderDetails(od);

	return false;
}

bool DBD::DBhelper::InsertOrderDetails(DBDefines::OrderDetails& order_details)
{
	if (db == nullptr)
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString(R"(INSERT INTO order_details (odid, orderid, productid, unitprice, quantity, discount)
values (?, ?, ?, ?, ?, ?);)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}

	query->addBindValue(order_details.odid);
	query->addBindValue(order_details.orderid);
	query->addBindValue(order_details.productid);
	query->addBindValue(order_details.unit_price);
	query->addBindValue(order_details.quantity);
	query->addBindValue(order_details.discount);

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return false;
	}

	return true;
}

bool DBD::DBhelper::InsertSingleOrder(DBDefines::Order& order)
{
	if (db == nullptr)
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString(R"(
insert into orders (orderid, customerid, employeeid, orderdate, requireddate, shippeddate, shipvia, freight, shipname, shipaddress, shipcity, shipregion, shippostalcode, shipcountry)
values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?);
)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}

	query->addBindValue(order.id);
	query->addBindValue(QString(order.customer_id.c_str()));
	query->addBindValue(order.employee_id);
	query->addBindValue(order.order_date);
	query->addBindValue(order.required_date);
	query->addBindValue(order.shipped_date);
	query->addBindValue(order.ship_via);
	query->addBindValue(0);
	query->addBindValue(QString(order.ship_name.c_str()));
	query->addBindValue(QString(order.ship_address.c_str()));
	query->addBindValue(QString(order.ship_city.c_str()));
	query->addBindValue(QString(order.ship_region.c_str()));
	query->addBindValue(QString(order.ship_postcode.c_str()));
	query->addBindValue(QString(order.ship_country.c_str()));

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return false;
	}

	return true;
}

/*
*				GetProduct
*/
bool DBD::DBhelper::InsertProduct(DBDefines::Product& p)
{
	if (db == nullptr)
		return false;

	auto id_opt = getMaxId("productid", "products");
	int id;
	if (id_opt.has_value())
		id = id_opt.value() + 1;
	else
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("SELECT * from InsertProduct(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}

	//query succeed
	query->addBindValue(id);
	query->addBindValue(QString(p.name.c_str()));
	query->addBindValue(p.supplier_id);
	query->addBindValue(p.category_id);
	query->addBindValue(QString(p.quantity_per_unit.c_str()));
	query->addBindValue(p.unit_price);
	query->addBindValue(p.units_in_stock);
	query->addBindValue(p.units_on_order);
	query->addBindValue(p.reorder_level);
	query->addBindValue(p.discontinued ? 1 : 0);

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return false;
	}

	if (query->next() == false)
		return false;

	p.id = query->value(0).toInt();

	return result;
}

/*
*				GetProduct
*/
std::optional<DBDefines::Product> DBD::DBhelper::GetProduct(int prod_id)
{
	if (db == nullptr)
		return std::nullopt;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("SELECT * from products where productid = ?")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return std::nullopt;
	}

	query->addBindValue(prod_id);

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return std::nullopt;
	}

	
	while (query->next())
	{
		DBDefines::Product p;
		using namespace DBDefines::Fields;
		p.id = query->value(PF_ID).toInt();
		p.name = query->value(PF_PRODUCTNAME).toString().toStdString();
		p.supplier_id = query->value(PF_SUPPLIER_ID).toInt();
		p.category_id = query->value(PF_CATEGORY_ID).toInt();
		p.quantity_per_unit = query->value(PF_QUANTITY_PER_UNIT).toString().toStdString();
		p.unit_price = query->value(PF_UNIT_PRICE).toInt();
		p.units_in_stock = query->value(PF_UNITS_IN_STOCK).toInt();
		p.units_on_order = query->value(PF_UNITS_ON_ORDER).toInt();
		p.reorder_level = query->value(PF_REORDER_LEVEL).toInt();
		p.discontinued = query->value(PF_DISCONTINUED).toInt();
		return p;
	}

	return std::nullopt;
}

std::optional<DBDefines::Customer> DBD::DBhelper::GetCustomer(QString cust_id)
{
	if (db == nullptr)
		return std::nullopt;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("SELECT * from customers where customerid = ?")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return std::nullopt;
	}

	query->addBindValue(cust_id);

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return std::nullopt;
	}

	while (query->next())
	{
		DBDefines::Customer c;
		using namespace DBDefines::Fields;

		c.id = query->value(0).toString();
		c.companyname = query->value(1).toString();
		c.contactname = query->value(2).toString();
		c.contanttitle = query->value(3).toString();
		c.address = query->value(4).toString();
		c.city = query->value(5).toString();
		c.region = query->value(6).toString();
		c.postcode = query->value(7).toString();
		c.country = query->value(8).toString();
		c.phone = query->value(9).toString();
		c.fax = query->value(10).toString();

		return c;
	}

	return std::nullopt;
}

std::optional<bool> DBD::DBhelper::CheckProductAvailability(int prod_id, int amount)
{
	if (db == nullptr)
		return std::nullopt;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("select count(*) from products where productid = ? and unitsinstock >= ?")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return std::nullopt;
	}
	else
	{
		query->addBindValue(prod_id);
		query->addBindValue(amount);
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return std::nullopt;
	}

	if (query->next() == false)
		return false;

	return (query->value(0).toInt() > 0) ? true : false;
}

int DBD::DBhelper::UpdateOrder(DBDefines::Order& order)
{
	if (db == nullptr)
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("SELECT * FROM UpdateOrder(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}
	else
	{
		query->addBindValue(order.id);
		query->addBindValue(order.customer_id.c_str());
		query->addBindValue(order.employee_id);
		query->addBindValue(order.order_date);
		query->addBindValue(order.required_date);
		query->addBindValue(order.shipped_date);
		query->addBindValue(order.ship_via);
		query->addBindValue(order.freight.c_str());
		query->addBindValue(order.ship_address.c_str());
		query->addBindValue(order.ship_city.c_str());
		query->addBindValue(order.ship_region.c_str());
		query->addBindValue(order.ship_postcode.c_str());
		query->addBindValue(order.ship_country.c_str());
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return false;
	}

	if (query->next() == false)
		return false;

	return (query->value(0).toInt() > 0) ? true : false;
}

int DBD::DBhelper::GetProductsFromCountry(std::vector<DBDefines::Product>& products, std::string country)
{
	if (db == nullptr)
		return 0;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString(R"(SELECT p.productid, p.productname, p.supplierid, p.categoryid, p.quantityperunit, p.unitprice, p.unitsinstock, p.unitsonorder, p.reorderlevel, p.discontinued FROM products p, suppliers s
WHERE s.supplierid=p.supplierid
AND s.country=?;)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return 0;
	}

	query->addBindValue(country.c_str());

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return 0;
	}

	
	while (query->next())
	{
		DBDefines::Product p;
		using namespace DBDefines::Fields;
		p.id = query->value(PF_ID).toInt();
		p.name = query->value(PF_PRODUCTNAME).toString().toStdString();
		p.supplier_id = query->value(PF_SUPPLIER_ID).toInt();
		p.category_id = query->value(PF_CATEGORY_ID).toInt();
		p.quantity_per_unit = query->value(PF_QUANTITY_PER_UNIT).toString().toStdString();
		p.unit_price = query->value(PF_UNIT_PRICE).toInt();
		p.units_in_stock = query->value(PF_UNITS_IN_STOCK).toInt();
		p.units_on_order = query->value(PF_UNITS_ON_ORDER).toInt();
		p.reorder_level = query->value(PF_REORDER_LEVEL).toInt();
		p.discontinued = query->value(PF_DISCONTINUED).toInt();

		products.push_back(p);
	}
}

int DBD::DBhelper::GetAllProducts(std::vector<DBDefines::Product>& products)
{
	if (db == nullptr)
		return 0;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString(R"(SELECT * FROM products;)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return 0;
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return 0;
	}


	while (query->next())
	{
		DBDefines::Product p;
		using namespace DBDefines::Fields;
		p.id = query->value(PF_ID).toInt();
		p.name = query->value(PF_PRODUCTNAME).toString().toStdString();
		p.supplier_id = query->value(PF_SUPPLIER_ID).toInt();
		p.category_id = query->value(PF_CATEGORY_ID).toInt();
		p.quantity_per_unit = query->value(PF_QUANTITY_PER_UNIT).toString().toStdString();
		p.unit_price = query->value(PF_UNIT_PRICE).toInt();
		p.units_in_stock = query->value(PF_UNITS_IN_STOCK).toInt();
		p.units_on_order = query->value(PF_UNITS_ON_ORDER).toInt();
		p.reorder_level = query->value(PF_REORDER_LEVEL).toInt();
		p.discontinued = query->value(PF_DISCONTINUED).toInt();

		products.push_back(p);
	}
}

int DBD::DBhelper::GetAllOrders(std::vector<DBDefines::Order>& orders)
{
	if (db == nullptr)
		return 0;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString(R"(SELECT * FROM orders;)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return 0;
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return 0;
	}


	while (query->next())
	{
		DBDefines::Order o;
		using namespace DBDefines::Fields;


		o.id = query->value(0).toInt();
		o.customer_id = query->value(1).toString().toStdString();
		o.employee_id = query->value(2).toInt();
		o.order_date = query->value(3).toDateTime();
		o.required_date = query->value(4).toDateTime();
		o.shipped_date = query->value(5).toDateTime();
		o.ship_via = query->value(6).toInt();
		o.freight = query->value(7).toString().toStdString();
		o.ship_name = query->value(8).toString().toStdString();
		o.ship_address = query->value(9).toString().toStdString();
		o.ship_city = query->value(10).toString().toStdString();
		o.ship_region = query->value(11).toString().toStdString();
		o.ship_postcode = query->value(12).toString().toStdString();
		o.ship_country = query->value(13).toString().toStdString();


		orders.push_back(o);
	}
}

int DBD::DBhelper::GetAllCustomers(std::vector<DBDefines::Customer>& customers)
{
	if (db == nullptr)
		return 0;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString(R"(SELECT * FROM customers;)")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return 0;
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return 0;
	}


	while (query->next())
	{
		DBDefines::Customer c;
		using namespace DBDefines::Fields;

		c.id = query->value(0).toString();
		c.companyname = query->value(1).toString();
		c.contactname = query->value(2).toString();
		c.contanttitle = query->value(3).toString();
		c.address = query->value(4).toString();
		c.city = query->value(5).toString();
		c.region = query->value(6).toString();
		c.postcode = query->value(7).toString();
		c.country = query->value(8).toString();
		c.phone = query->value(9).toString();
		c.fax = query->value(10).toString();

		customers.push_back(c);
	}

	return customers.size();
}

int DBD::DBhelper::DeleteEntry(std::string table_name, std::string id_name, int id)
{
	if (db == nullptr)
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	std::stringstream ss;
	ss << "DELETE FROM " << table_name << " WHERE " << id_name << " = ?;";

	if (!query->prepare(QString(ss.str().c_str())))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}
	else
	{
		query->addBindValue(id);
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return 0;
	}

	return 1;
}

int DBD::DBhelper::getSuppliersCountries(std::vector<std::string>& countries)
{
	if (db == nullptr)
		return 0;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	if (!query->prepare(QString("select distinct country from suppliers")))
	{
		qDebug() << "Error = " << db->lastError().text();
		return 0;
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return 0;
	}

	while (query->next())
	{
		countries.push_back(query->value(0).toString().toStdString());

		/*DBDefines::Product p;
		using namespace DBDefines::Fields;
		p.id = query->value(PF_ID).toInt();
		p.name = query->value(PF_PRODUCTNAME).toString().toStdString();
		p.supplier_id = query->value(PF_SUPPLIER_ID).toInt();
		p.category_id = query->value(PF_CATEGORY_ID).toInt();
		p.quantity_per_unit = query->value(PF_QUANTITY_PER_UNIT).toString().toStdString();
		p.unit_price = query->value(PF_UNIT_PRICE).toInt();
		p.units_in_stock = query->value(PF_UNITS_IN_STOCK).toInt();
		p.units_on_order = query->value(PF_UNITS_ON_ORDER).toInt();
		p.reorder_level = query->value(PF_REORDER_LEVEL).toInt();
		p.discontinued = query->value(PF_DISCONTINUED).toInt();

		products.push_back(p);*/
	}

	return countries.size();
}

/*
*				getMaxId
*/
std::optional<int> DBD::DBhelper::getMaxId(std::string id_name, std::string table)
{
	if (db == nullptr)
		return std::nullopt;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	std::stringstream ss;
	ss << "SELECT max(" << id_name << ") from " << table;
	std::string s =	ss.str();
	if (!query->prepare(QString(s.c_str())))
	{
		qDebug() << "Error = " << db->lastError().text();
		return std::nullopt;
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return std::nullopt;
	}

	if (query->next() == false)
		return std::nullopt;

	return query->value(0).toInt();
}
