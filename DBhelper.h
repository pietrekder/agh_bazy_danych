#pragma once

#include "DBDefines.h"

//#include "QtSql/qtsqlglobal.h"
#include "QtSql/qsqldatabase.h"

#include "qtsqlglobal.h"
#include "qsqlquery.h"
#include "qsqlerror"
#include "qdatetime.h"
#include "qdebug.h"
#include "qstring.h"

#include <sstream>

#include <memory>
#include <variant>
#include <sstream>

namespace DBD
{

class DBhelper : public DBDefines::IDBAccess
{
public:
    DBhelper(const char* driver);
    virtual ~DBhelper() = default;

    //overrides from IDBAccess
    bool Connect(const char* server,
                 const char* dbName,
                 const char* user,
                 const char* pwd) override;
    void Disconnect() override;
    bool IsOpen() override;

    //CRUD overrides from IDBAccess
	//  INSERT
    bool InsertSupplier(DBDefines::Supplier& s) override;
    std::optional<DBDefines::Supplier> GetSupplier(int sup_id) override;

	bool InsertOrder(DBDefines::Order& o, std::vector<DBDefines::OrderDetails>& order_details);
	bool InsertOrderDetails(DBDefines::OrderDetails& order_details);
	bool InsertSingleOrder(DBDefines::Order& order);

	//  INSERT
    bool InsertProduct(DBDefines::Product& p);
    std::optional<DBDefines::Product> GetProduct(int prod_id);

	std::optional<DBDefines::Customer> GetCustomer(QString cust_id);

	std::optional<bool> CheckProductAvailability(int prod_id, int amount);

	//  UPDATE
    int UpdateOrder(DBDefines::Order& order);

	//  UPDATE
    template <typename T>
    int UpdateTable(std::string table, std::string param, T value, std::string id_name, int id);

	//  READ
    int GetProductsFromCountry(std::vector<DBDefines::Product>& products, std::string country);

	int GetAllProducts(std::vector<DBDefines::Product>& products);

	int GetAllOrders(std::vector<DBDefines::Order>& orders);

	int GetAllCustomers(std::vector<DBDefines::Customer>& customers);

	//  DELETE
	int DeleteEntry(std::string table_name, std::string id_name, int id);


	int getSuppliersCountries(std::vector<std::string>& countries);

private:
    std::optional<int> getMaxId(std::string id_name, std::string table);

private:
    std::unique_ptr<QSqlDatabase> db = nullptr;     //! database object
};


template <typename T>
int DBhelper::UpdateTable(std::string table, std::string param, T value, std::string id_name, int id)
{
	if (db == nullptr)
		return false;

	std::unique_ptr<QSqlQuery> query = std::make_unique<QSqlQuery>(*db);

	std::stringstream ss;
	ss << "UPDATE " << table << " Set " << param << " = ? where " << id_name << " = ?;";

	if (!query->prepare(QString(ss.str().c_str())))
	{
		qDebug() << "Error = " << db->lastError().text();
		return false;
	}
	else
	{
		if constexpr (std::is_same_v<T, std::string>)
			query->addBindValue(value.c_str());
		else
			query->addBindValue(value);
		query->addBindValue(id);
	}

	bool result = query->exec();

	if (query->lastError().type() != QSqlError::NoError || !result)
	{
		qDebug() << query->lastError().text();
		return false;
	}

	if (query->next() == false)
		return false;

	return (query->value(0).toInt() > 0) ? true : false;
}

}