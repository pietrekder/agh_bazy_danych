/*  SUPPLIERS TABLE  */
CREATE TABLE suppliers (
  SupplierID SERIAL NOT NULL,
  CompanyName varchar(40) default NULL,
  ContactName varchar(30) default NULL,
  ContactTitle varchar(30) default NULL,
  Address varchar(60) default NULL,
  City varchar(15) default NULL,
  Region varchar(15) default NULL,
  PostalCode varchar(10) default NULL,
  Country varchar(15) default NULL,
  Phone varchar(24) default NULL,
  Fax varchar(24) default NULL,
  HomePage text,
  PRIMARY KEY  (SupplierID)
);

/*  PRODUCTS TABLE  */
CREATE TABLE products (
  ProductID SERIAL NOT NULL,
  ProductName varchar(40) default NULL,
  SupplierID int default NULL,
  CategoryID int default NULL,
  QuantityPerUnit varchar(20) default NULL,
  UnitPrice float default '0',
  UnitsInStock smallint default '0',
  UnitsOnOrder smallint default '0',
  ReorderLevel smallint default '0',
  Discontinued smallint default '0',
  PRIMARY KEY  (ProductID)
);


/*  CATEGORIES TABLE  */
CREATE TABLE categories (
  CategoryID SERIAL NOT NULL,
  CategoryName varchar(15) default NULL,
  Description text,
  Picture varchar(40) default NULL,
  PRIMARY KEY  (CategoryID)
);

ALTER TABLE products ADD FOREIGN KEY (SupplierID) REFERENCES suppliers (SupplierID);
ALTER TABLE products ADD FOREIGN KEY (CategoryID) REFERENCES categories (CategoryID);


/*  REGION TABLE  */
CREATE TABLE region (
  regionid SERIAL PRIMARY KEY,
  region_desc varchar
);

/*  REGION TERRITORIES TABLE  */
CREATE TABLE territories (
  territoryid SERIAL PRIMARY KEY,
  terrdesc_region_id int NOT NULL
);

ALTER TABLE territories ADD FOREIGN KEY (terrdesc_region_id) REFERENCES region (regionid);


/*  EMPLOYEES TABLE  */
CREATE TABLE employees (
  EmployeeID SERIAL NOT NULL,
  LastName varchar(20) default NULL,
  FirstName varchar(10) default NULL,
  Title varchar(30) default NULL,
  TitleOfCourtesy varchar(25) default NULL,
  BirthDate date default NULL,
  HireDate date default NULL,
  Address varchar(60) default NULL,
  City varchar(15) default NULL,
  Region varchar(15) default NULL,
  PostalCode varchar(10) default NULL,
  Country varchar(15) default NULL,
  HomePhone varchar(24) default NULL,
  Extension varchar(4) default NULL,
  Photo varchar(40) default NULL,
  Notes text,
  ReportsTo int default NULL,
  PRIMARY KEY  (EmployeeID)
);

/*  EMPLOYEE TERRITORIES TABLE  */
CREATE TABLE employee_territories (
  employee_id integer PRIMARY KEY NOT NULL,
  territory_id integer NOT NULL
);

ALTER TABLE employee_territories ADD FOREIGN KEY (employee_id) REFERENCES employees (EmployeeID);
ALTER TABLE employee_territories ADD FOREIGN KEY (territory_id) REFERENCES territories (territoryid);


/*  CUSTOMER DEMOGRAPHICS TABLE  */
CREATE TABLE customer_demographics (
  id SERIAL PRIMARY KEY,
  customer_desc varchar
);

/*  CUSTOMER TABLE  */
CREATE TABLE customers (
  CustomerID varchar(5) NOT NULL default '',
  CompanyName varchar(40) default NULL,
  ContactName varchar(30) default NULL,
  ContactTitle varchar(30) default NULL,
  Address varchar(60) default NULL,
  City varchar(15) default NULL,
  Region varchar(15) default NULL,
  PostalCode varchar(10) default NULL,
  Country varchar(15) default NULL,
  Phone varchar(24) default NULL,
  Fax varchar(24) default NULL,
  PRIMARY KEY  (CustomerID)
);


/*  CUSTOMER CUSTOMER DEMO TABLE  */
CREATE TABLE customercustomer_demo (
  id integer PRIMARY KEY NOT NULL,
  type_id integer NOT NULL
);

ALTER TABLE customercustomer_demo ADD FOREIGN KEY (id) REFERENCES customers (CustomerID);
ALTER TABLE customercustomer_demo ADD FOREIGN KEY (type_id) REFERENCES customer_demographics (id);


/*  SHIPPERS TABLE  */
CREATE TABLE shippers (
  ShipperID SERIAL NOT NULL,
  CompanyName varchar(40) default NULL,
  Phone varchar(24) default NULL,
  PRIMARY KEY  (ShipperID)
);

/*  ORDERS TABLE  */
CREATE TABLE orders (
  OrderID SERIAL NOT NULL,
  CustomerID varchar(5) default NULL,
  EmployeeID int default NULL,
  OrderDate date default NULL,
  RequiredDate date default NULL,
  ShippedDate date default NULL,
  ShipVia int default NULL,
  Freight float default '0',
  ShipName varchar(40) default NULL,
  ShipAddress varchar(60) default NULL,
  ShipCity varchar(15) default NULL,
  ShipRegion varchar(15) default NULL,
  ShipPostalCode varchar(10) default NULL,
  ShipCountry varchar(15) default NULL,
  PRIMARY KEY  (OrderID)
);

ALTER TABLE orders ADD FOREIGN KEY (CustomerID) REFERENCES customers (CustomerID);
ALTER TABLE orders ADD FOREIGN KEY (EmployeeID) REFERENCES employees (EmployeeID);
ALTER TABLE orders ADD FOREIGN KEY (ShipVia) REFERENCES shippers (ShipperID);


/*  ORDER DETAILS TABLE  */
CREATE TABLE order_details (
  odID SERIAL NOT NULL ,
  OrderID int default '0',
  ProductID int default '0',
  UnitPrice float default '0',
  Quantity smallint default '1',
  Discount float default '0',
  PRIMARY KEY  (odID)
);

ALTER TABLE order_details ADD FOREIGN KEY (OrderID) REFERENCES orders (OrderID);
ALTER TABLE order_details ADD FOREIGN KEY (ProductID) REFERENCES products (ProductID);
