#include "OrdersWidget.h"

#include <qmessagebox.h>

OrdersWidget::OrdersWidget(QWidget *parent)
    : QWidget(parent)
{
    ui.setupUi(this);
    model = new OrderModel();

    ui.tableView->setModel(model);

    connect(ui.btn_all_orders, &QPushButton::pressed, this, [&]() { listOrders(); });
    connect(ui.btn_makeorder, &QPushButton::pressed, this, [&]() { makeOrder(); });

}

OrdersWidget::~OrdersWidget()
{
}

void OrdersWidget::listOrders()
{
    std::vector<DBDefines::Order> orders;
    helper->GetAllOrders(orders);

    for (auto& o : orders)
        model->insertOrder(o);
}

void OrdersWidget::makeOrder()
{
    ui.errors_lbl->setText("");
    auto cust = helper->GetCustomer(ui.le_customer->text());
    if (cust.has_value() == false)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Customer error");
        msgBox.setText("Customer o podanym id nie istnieje!");
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
        return;
    }

    auto customer = cust.value();

    auto product_check = [&](int pid, int amount)->bool {
        auto p1available = helper->CheckProductAvailability(pid, amount);
        if (p1available.has_value() == false)
        {
            QMessageBox msgBox;
            msgBox.setWindowTitle("Product query error");
            msgBox.setText("Wewnętrzny blad zapytania");
            msgBox.setDefaultButton(QMessageBox::Ok);
            msgBox.exec();
            return false;
        }

        return p1available.value();
    };

    if(!product_check(ui.le_p1->text().toInt(), ui.le_p1amo->text().toInt()))
    {
        ui.errors_lbl->setText("Za malo produktu 1 w magazynie");
        return;
    }
    if(!product_check(ui.le_p2->text().toInt(), ui.le_p2amo->text().toInt()))
    {
        ui.errors_lbl->setText("Za malo produktu 2 w magazynie");
        return;
    }


    auto p1 = helper->GetProduct(ui.le_p1->text().toInt());

    if (p1.has_value() == false)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Product error");
        msgBox.setText("Produkt 1 o podanym id nie istnieje!");
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
        return;
    }
    auto product1 = p1.value();

    auto p2 = helper->GetProduct(ui.le_p2->text().toInt());

    if (p2.has_value() == false)
    {
        QMessageBox msgBox;
        msgBox.setWindowTitle("Product error");
        msgBox.setText("Produkt 2 o podanym id nie istnieje!");
        msgBox.setDefaultButton(QMessageBox::Ok);
        msgBox.exec();
        return;
    }
    auto product2 = p2.value();

    DBDefines::Order o;
    o.customer_id = customer.id.toStdString();
    o.employee_id = 8;
    o.freight = "";
    o.order_date = QDateTime::currentDateTime();
    o.required_date = QDateTime::currentDateTime();
    o.required_date = o.required_date.addDays(3);
    o.shipped_date = QDateTime::currentDateTime();
    o.ship_address = customer.address.toStdString();
    o.ship_city = customer.city.toStdString();
    o.ship_name = "ship name";
    o.ship_country = customer.country.toStdString();
    o.ship_postcode = customer.postcode.toStdString();
    o.ship_region = customer.region.toStdString();
    o.ship_via = 2;

    std::vector<DBDefines::OrderDetails> order_details;
    DBDefines::OrderDetails o1;
    o1.discount = 0;
    o1.productid = product1.id;
    o1.quantity = ui.le_p1amo->text().toInt();
    o1.unit_price = product1.unit_price;
    order_details.push_back(o1);

    DBDefines::OrderDetails o2;
    o2.discount = 0;
    o2.productid = product2.id;
    o2.quantity = ui.le_p2amo->text().toInt();
    o2.unit_price = product2.unit_price;
    order_details.push_back(o2);

    int neworder_id = helper->InsertOrder(o, order_details);
}
